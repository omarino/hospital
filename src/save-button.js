export const birthDay = document.getElementById("birth-day");
export const bloodyGroup = document.getElementById("bloody-group");
export const city = document.getElementById("city");
export const gender = document.getElementById("gender");
export const name = document.getElementById("name");
export const region = document.getElementById("region");
export const residency = document.getElementById("residency");
export const rh = document.getElementById("rh");
export const state = document.getElementById("state");
export const streetNum = document.getElementById("street-num");
export const surname = document.getElementById("surname");

import { createPatient } from "./functions/patients";
import { currentPatient} from "./hospital";
import { taxCode } from "./functions/tax-code";
import { updatePatient } from "./functions/patients";
import { Patient } from "./models/patient";

const save = document.getElementById("save");

save.addEventListener("click", e => {
  e.preventDefault();
  if (!currentPatient) {
    const p = new Patient(
      0,
      taxCode.value,
      name.value,
      surname.value,
      bloodyGroup.value,
      rh.value,
      gender.value,
      birthDay.value,
      city.value,
      region.value,
      state.value,
      residency.value,
      streetNum.value,
    );
    createPatient(p);

  } else if (currentPatient) {
      const p = new Patient(
      currentPatient.id,
      taxCode.value,
      name.value,
      surname.value,
      bloodyGroup.value,
      rh.value,
      gender.value,
      birthDay.value,
      city.value,
      region.value,
      state.value,
      residency.value,
      streetNum.value,
    );
    updatePatient(p);
  }; 
});

