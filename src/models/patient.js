export class Patient {
  constructor(
    id,
    taxCode,
    name,
    surname,
    bloodyGroup,
    rh,
    gender,
    birthDay,
    city,
    country,
    state,
    streetName,
    streetNumber
  ) {
    this.birthDay = birthDay;
    this.city = city;
    this.country = country;
    this.gender = gender;
    this.id = id;
    this.name = name;
    this.state = state;
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.surname = surname;
    this.taxCode = taxCode;
    this.bloodyGroup = bloodyGroup;
    this.rh = rh;
  }
}

  