import {Patient} from '../models/patient';

function objectModel(data) {
    return new Patient(
        data.id,
        data.tax_code,
        data.name,
        data.surname,
        data.bloody_group,
        data.rh,
        data.gender,
        data.birth_day,
        data.address.city,
        data.address.country,
        data.address.state,
        data.address.street_name,
        data.address.street_number,
    )
}
   
export const getPatients = async taxCode => {
    const response = await fetch(`http://localhost:3000/people?tax_code=${taxCode}`);
    const patients = await response.json();

    return patients.map(obj => (objectModel(obj)));


};


export const updatePatient = async (patient) => {

    const response = await fetch(`http://localhost:3000/people/${patient.id}`, {
        body: JSON.stringify({
          address: {
            city: patient.city,
            country: patient.country,
            state: patient.state,
            street_name: patient.streetName,
            street_number: patient.streetNumber,
           },
            birth_day: patient.birthDay,
            name: patient.name,
            surname: patient.surname,
            tax_code: patient.taxCode,
            bloody_group: patient.bloodyGroup,
            rh: patient.rh,
            gender: patient.gender
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        method: 'PUT',
    });
    
    const data = await response.json();

    if(response.status === 200) {
        alert("Modifica dei dati effettuata corretamente !");
    }
    return objectModel(data);
};



export const createPatient = async (patient) => {

    const response = await fetch(`http://localhost:3000/people`, {
        body: JSON.stringify({
          address: {
            city: patient.city,
            country: patient.country,
            state: patient.state,
            street_name: patient.streetName,
            street_number: patient.streetNumber,
           },
            birth_day: patient.birthDay,
            name: patient.name,
            surname: patient.surname,
            tax_code: patient.taxCode,
            bloody_group: patient.bloodyGroup,
            rh: patient.rh,
            gender: patient.gender
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        method: 'POST',
    });

    const data = await response.json();

    if (response.status === 201) {
        alert("Salvataggio dei dati effettuato correttamente !");
    }
    return objectModel(data);
};