export const taxCode = document.getElementById("cf");

export function taxCodeValidator(e) {
  let regex = /^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]{1}/;
  if (regex.test(e) && e.length < 17) {
    let error = taxCode.nextElementSibling;
    if (error) {
      error.remove();
    }
    return true;
  }
  if (taxCode.nextElementSibling) {
    return false;
  }
  let error2 = document.createElement("div");
  error2.innerText = "Il Codice Fiscale inserito non è valido";
  error2.className = "error";
  taxCode.parentElement.appendChild(error2);

  return false;
}
