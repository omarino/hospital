const date = document.getElementById("date");
let options = { year: "numeric", month: "2-digit", day: "numeric" };

export const currentTime = () => {
  date.innerText =
    "Data:" +
    new Date().toLocaleDateString("it-IT", options) +
    " Ora:" +
    new Date().toLocaleTimeString("it-IT", {
      hour: "2-digit",
      minute: "2-digit",
    });
};
