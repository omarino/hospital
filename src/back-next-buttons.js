import { homePage, personalData } from "./color-buttons";
import { taxCode, taxCodeValidator } from "./functions/tax-code";

const specification = document.getElementById("step3");
const handleBackNextBtns = document.querySelectorAll(".btn-change");

const mandatory = () => {
  if (taxCodeValidator(taxCode.value)) {
    return true;
  }

  return false;
};

handleBackNextBtns.forEach(element => element.addEventListener("click", e => {
  if (e.target.dataset.class === "back") {
    const white = document.querySelector('div[class="white"]');
    const green = document.querySelector('div[class="green"]');
    const cyan = document.querySelector('div[class="cyan"]');

    homePage.hidden = false;
    personalData.hidden = true;
    specification.hidden = true;

    document.getElementById("emergency").remove();
    if (cyan || green || white) {
      document.getElementById("mdt").remove();
    }
  }
  else if (e.currentTarget.dataset.class === "next") {
    const cyanD = document.querySelector('div[class="cyan"]');
    const greenD = document.querySelector('div[class="green"]');
    const whiteD = document.querySelector('div[class="white"]');
  
    if ((cyanD || greenD || whiteD) && !mandatory()) {
      return;
    }
  
    homePage.hidden = true;
    personalData.hidden = true;
    specification.hidden = false;
  }
  else if (e.target.dataset.class === "previous") {
    homePage.hidden = true;
    personalData.hidden = false;
    specification.hidden = true;
  }
}));

  