export const cyan = document.getElementById("cyan");
const emergencyType = document.getElementById("emergency-type");
export const green = document.getElementById("green");
const handleColorBtns = document.querySelectorAll(".btn");
export const homePage = document.getElementById("step1");
export const important = document.getElementById("important");
export const personalData = document.getElementById("step2");
export const white = document.getElementById("white");

export const handleTypeEmergency = (classes, color, emergencyCode) => {
    emergencyType.innerHTML = ` 
       <div data-class="${classes} "class="${classes}" id ="emergency">
           <div class="code-patient">Codice Paziente:</div>
           <div class="emergency">${color}: ${emergencyCode}</div>
       </div>
    `;
};

const mandatoryMessage = () => {
    important.innerHTML = `
       <div class="mandatory" id="mdt">
          <p>Il Codice Fiscale è obbligatorio per poter proseguire</p>
       </div> 
    `;
};

const handleHidden = () => {
    homePage.hidden = true;
    personalData.hidden = false;
}

handleColorBtns.forEach(element => element.addEventListener("click", e => {
   handleHidden();
   handleTypeEmergency(e.currentTarget.dataset.class, e.currentTarget.dataset.title, e.currentTarget.dataset.message);

   if (['cyan', 'green', 'white'].includes(e.currentTarget.dataset.class)) {
      mandatoryMessage();
   }
   
}));

