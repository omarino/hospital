import "core-js/stable";
import "regenerator-runtime/runtime";

import { birthDay, bloodyGroup, city, gender, name,
region, residency, rh, state, streetNum, surname} from "./save-button";
import { currentTime } from "./functions/current-time";
import { getPatients } from "./functions/patients";
import {important} from "./color-buttons"
import {taxCode} from "./functions/tax-code";
import { taxCodeValidator } from "./functions/tax-code";

export let currentPatient;

setInterval(currentTime(), 200);

taxCode.addEventListener('change', async e => {
  e.preventDefault
  const patients = await getPatients(e.target.value);
    if (taxCodeValidator(taxCode.value)) {
      name.value = patients[0].name;
      surname.value = patients[0].surname
      state.value = patients[0].state;
      region.value = patients[0].country;
      city.value = patients[0].city;
      residency.value = patients[0].streetName;
      streetNum.value = patients[0].streetNumber;
      birthDay.value = patients[0].birthDay;
      bloodyGroup.value = patients[0].bloodyGroup;
      rh.value = patients[0].rh;
      gender.value = patients[0].gender;

      currentPatient = patients[0];
      important.remove();
    }
    else if (!taxCodeValidator(taxCode.value)) {
      name.value = "";
      surname.value = "";
      state.value = "";
      region.value = "";
      city.value = "";
      residency.value = "";
      streetNum.value = "";
      birthDay.value = "";
      bloodyGroup.value = "bloodyGroup";
      rh.value = "RH";
      gender.value = "gender";

      currentPatient = undefined;
    }
});

$(document).on("ready", function () {
  $(".js-example-basic-multiple").select2({});
});

$(".js-example-responsive").select2({
  width: "resolve",
});

$(".js-example-tags").select2({
  tags: true,
});

$(".js-example-basic-multiple").select2({
  placeholder: {
    id: "-1",
    text: "Farmaci assunti di recente: ",
  },
});
  
document.querySelector('form[name="specification"]').addEventListener("submit", function (e) {
  e.preventDefault();
});