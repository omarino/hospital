module.exports = {
    devtool: 'source-map',
    entry: {
        main: './src/hospital.js',
        menu: './src/menu.js',
        "color-buttons": './src/color-buttons.js',
        "back-next-buttons": './src/back-next-buttons.js',
        "save-button": './src/save-button.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}